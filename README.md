# Meta vlab
This project holds the meta layer for building a raspberry pi based vlab.
It automatically installs all the necessary packages and creates a bootfs/rootfs tarball for use with PXE boot.

## Building
Meta vlab is meant to be build using [kas](https://github.com/siemens/kas) and `core-image-minimal` target should be used for building.
The bootfs tarball is located inside `build/tmp/deploy/images/raspberrypi4-64/raspberrypi4-64.bootfs.tar`
