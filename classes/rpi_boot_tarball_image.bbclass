inherit image_types

#
# Create an image that can be written onto a SD card using dd.
#
# The disk layout used is:
#
#    0                      -> IMAGE_ROOTFS_ALIGNMENT         - reserved for other data
#    IMAGE_ROOTFS_ALIGNMENT -> BOOT_SPACE                     - bootloader and kernel
#    BOOT_SPACE             -> SDIMG_SIZE                     - rootfs
#

#                                                     Default Free space = 1.3x
#                                                     Use IMAGE_OVERHEAD_FACTOR to add more space
#                                                     <--------->
#            4MiB              48MiB           SDIMG_ROOTFS
# <-----------------------> <----------> <---------------------->
#  ------------------------ ------------ ------------------------
# | IMAGE_ROOTFS_ALIGNMENT | BOOT_SPACE | ROOTFS_SIZE            |
#  ------------------------ ------------ ------------------------
# ^                        ^            ^                        ^
# |                        |            |                        |
# 0                      4MiB     4MiB + 48MiB       4MiB + 48Mib + SDIMG_ROOTFS

# This image depends on the rootfs image
# IMAGE_TYPEDEP:rpi-boot-tarball = "${SDIMG_ROOTFS_TYPE}"

# Kernel image name
SDIMG_KERNELIMAGE:raspberrypi  ?= "kernel.img"
SDIMG_KERNELIMAGE:raspberrypi2 ?= "kernel7.img"
SDIMG_KERNELIMAGE:raspberrypi3-64 ?= "kernel8.img"

# For the names of kernel artifacts
inherit kernel-artifact-names

RPI_SDIMG_EXTRA_DEPENDS ?= ""

do_image_rpi_tarball[depends] = " \
    parted-native:do_populate_sysroot \
    mtools-native:do_populate_sysroot \
    dosfstools-native:do_populate_sysroot \
    virtual/kernel:do_deploy \
    rpi-bootfiles:do_deploy \
    ${@bb.utils.contains('MACHINE_FEATURES', 'armstub', 'armstubs:do_deploy', '' ,d)} \
    ${@bb.utils.contains('RPI_USE_U_BOOT', '1', 'u-boot:do_deploy', '',d)} \
    ${@bb.utils.contains('RPI_USE_U_BOOT', '1', 'u-boot-default-script:do_deploy', '',d)} \
    ${RPI_SDIMG_EXTRA_DEPENDS} \
"

do_image_rpi_boot_tarball[recrdeps] = "do_build"

# Additional files and/or directories to be copied into the vfat partition from the IMAGE_ROOTFS.
IMGPAYLOAD ?= ""

IMAGE_CMD:rpi-boot-tarball () {
    # Check if we are building with device tree support
    DTS="${@make_dtb_boot_files(d)}"
    BOOTFS_DIR=${WORKDIR}/out/bootfs
    mkdir -p ${BOOTFS_DIR}

    cp -rL ${DEPLOY_DIR_IMAGE}/${BOOTFILES_DIR_NAME}/* ${BOOTFS_DIR} || bbfatal "cp cannot copy ${DEPLOY_DIR_IMAGE}/${BOOTFILES_DIR_NAME}/* into ${BOOTFS_DIR}"
    if [ "${@bb.utils.contains("MACHINE_FEATURES", "armstub", "1", "0", d)}" = "1" ]; then

        cp -rL ${DEPLOY_DIR_IMAGE}/armstubs/${ARMSTUB} ${BOOTFS_DIR} || bbfatal "cp cannot copy ${DEPLOY_DIR_IMAGE}/armstubs/${ARMSTUB} into ${BOOTFS_DIR}"
    fi
    if test -n "${DTS}"; then
        mkdir -p  ${BOOTFS_DIR}/overlays
        for entry in ${DTS} ; do
            # Split entry at optional ';'
            if [ $(echo "$entry" | grep -c \;) = "0" ] ; then
                DEPLOY_FILE="$entry"
                DEST_FILENAME="$entry"
            else
                DEPLOY_FILE="$(echo "$entry" | cut -f1 -d\;)"
                DEST_FILENAME="$(echo "$entry" | cut -f2- -d\;)"
            fi
            cp -rL  ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE}  ${BOOTFS_DIR} || bbfatal "cp cannot copy ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE} into ${BOOTFS_DIR}"
        done
    fi
    if [ "${RPI_USE_U_BOOT}" = "1" ]; then
        bbfatal "Unable to produce tarball when using uboot"
    else
        if [ ! -z "${INITRAMFS_IMAGE}" -a "${INITRAMFS_IMAGE_BUNDLE}" = "1" ]; then
            cp -rL ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}-${INITRAMFS_LINK_NAME}.bi ${BOOTFS_DIR}/${SDIMG_KERNELIMAGE} || bbfatal "cp cannot copy ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}-${INITRAMFS_LINK_NAME}.bin into } ${BOOTFS_DIR}"
        else
            cp -rL ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE}  ${BOOTFS_DIR}/${SDIMG_KERNELIMAGE} || bbfatal "mcopy cannot copy ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE} into ${BOOTFS_DIR}"
        fi
    fi

    # Add files (eg. hypervisor binaries) from the deploy dir
    if [ -n "${DEPLOYPAYLOAD}" ] ; then
        echo "Copying deploy file payload into tar"
        for entry in ${DEPLOYPAYLOAD} ; do
            # Split entry at optional ':' to enable file renaming for the destination
            if [ $(echo "$entry" | grep -c :) = "0" ] ; then
                DEPLOY_FILE="$entry"
                DEST_FILENAME="$entry"
            else
                DEPLOY_FILE="$(echo "$entry" | cut -f1 -d:)"
                DEST_FILENAME="$(echo "$entry" | cut -f2- -d:)"
            fi
            cp -rL ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE} ${BOOTFS_DIR}/${DEST_FILENAME}  || bbfatal "cp cannot copy ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE} into ${BOOTFS_DIR}/${DEST_FILENAME}"
        done
    fi

    if [ -n "${FATPAYLOAD}" ] ; then
        echo "Copying payload into VFAT"
        for entry in ${FATPAYLOAD} ; do
            # use bbwarn instead of bbfatal to stop aborting on vfat issues like not supporting .~lock files
            cp -rL  ${IMAGE_ROOTFS}$entry ${BOOTFS_DIR} || bbwarn "cp cannot copy ${IMAGE_ROOTFS}$entry into ${BOOTFS_DIR}"
        done
    fi

    # Add stamp file
    echo "${IMAGE_NAME}" > ${WORKDIR}/image-version-info
    cp -rL ${WORKDIR}/image-version-info ${BOOTFS_DIR} || bbfatal "cp cannot copy ${WORKDIR}/image-version-info into ${BOOTFS_DIR}"
    tar -C ${WORKDIR}/out/bootfs --sort=name --format=posix --numeric-owner -cf ${IMGDEPLOYDIR}/${IMAGE_NAME}.bootfs.tar . || [ $? -eq 1 ]
    ln -s -r ${IMGDEPLOYDIR}/${IMAGE_NAME}.bootfs.tar  ${IMGDEPLOYDIR}/${IMAGE_BASENAME}-${MACHINE}.bootfs.tar
}

ROOTFS_POSTPROCESS_COMMAND += " rpi_generate_sysctl_config ; "

rpi_generate_sysctl_config() {
    # systemd sysctl config
    test -d ${IMAGE_ROOTFS}${sysconfdir}/sysctl.d && \
        echo "vm.min_free_kbytes = 8192" > ${IMAGE_ROOTFS}${sysconfdir}/sysctl.d/rpi-vm.conf

    # sysv sysctl config
    IMAGE_SYSCTL_CONF="${IMAGE_ROOTFS}${sysconfdir}/sysctl.conf"
    test -e ${IMAGE_ROOTFS}${sysconfdir}/sysctl.conf && \
        sed -e "/vm.min_free_kbytes/d" -i ${IMAGE_SYSCTL_CONF}
    echo "" >> ${IMAGE_SYSCTL_CONF} && echo "vm.min_free_kbytes = 8192" >> ${IMAGE_SYSCTL_CONF}
}
