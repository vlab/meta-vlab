SUMMARY = "Easily forge ICMP packets and make your own ping and traceroute."
HOMEPAGE = "https://github.com/ValentinBELYN/icmplib"
LICENSE = "LGPL-3.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=ff3103b5db8ba4e2c66c511b7a73e407"

SRC_URI[sha256sum] = "e0a3452c489ae591364141503467bf387c11fbd5c3adaf0650621d8266efff53"

inherit pypi setuptools3
