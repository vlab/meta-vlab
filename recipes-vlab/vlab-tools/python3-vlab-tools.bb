DESCRIPTION = "Python tools for managing vlabs"
HOMEPAGE = "https://source.denx.de/vlab/vlab-tools/"
LICENSE = "GPL-3.0-only"
SECTION = "devel/python"
LIC_FILES_CHKSUM = "file://LICENSE.md;md5=368535a71ade0b0fee0783d8b9c9e3ea"

SRCREV = "30bf9866c4ecb833dc99e877ea8eafb1ffe803a8"
SRC_URI = "git://source.denx.de/vlab/vlab-tools;branch=main;protocol=https"

S = "${WORKDIR}/git"

inherit python_setuptools_build_meta

RDEPENDS:${PN} += " \
    python3-ifaddr \
    python3-pyserial \
    python3-tomli \
    python3-zeroconf \
    python3-pyudev \
    python3-icmplib \
    ttynvt \
"
