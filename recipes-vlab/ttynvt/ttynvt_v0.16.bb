SUMMARY = "Virtual Network Terminal supporting the Com Port Control Option (RFC2217)"
SECTION = "lib/tools"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263"

SRC_URI = "https://gitlab.com/lars-thrane-as/ttynvt/-/archive/${PV}/ttynvt-${PV}.tar.gz"
SRC_URI[sha256sum] = "a730874e13fe079782074637f691e455469fee8f3ae56cab678828b86eead084"
DEPENDS = "fuse"

inherit autotools pkgconfig
